#!/bin/bash -e

if [[ $EUID -ne 0 ]]; then
  echo "Must be run as the root user."
  exit 1
fi
export DEBIAN_FRONTEND=noninteractive
apt-get update -y
apt-get install -y python3-pip
python3 -m pip install -f sphinx sphinx_rtd_theme sphinx-intl
