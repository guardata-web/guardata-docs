
=============
guardata docs
=============

.. image:: _static/guardata_logo.png
    :align: center
    :alt: guardata logo

.. note::
    This documentation is available in French (soon) and English languages.
    Expand the "Read the Docs" panel at the bottom of the left sidebar to see the list.

|

guardata documentation
======================

> TO BE REDACTED <

.. toctree::
   :maxdepth: 0
   :name: sec-discover

   discover

.. toctree::
   :maxdepth: 1
   :caption: Start
   :name: sec-start

   start/installation
   start/startgroup

.. toctree::
   :maxdepth: 1
   :caption: User Guide
   :name: sec-guide

   userguide/index
   roles

.. toctree::
   :maxdepth: 1
   :caption: In depth
   :name: sec-advanced

   architecture
   cryptography

.. toctree::
   :maxdepth: 1
   :caption: Customization
   :name: sec-custom

   customization

.. toctree::
   :maxdepth: 1
   :caption: For Developers
   :name: sec-devel

   development

.. Indices and tables
.. ------------------
..
.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
