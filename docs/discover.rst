.. _doc_discover:

========
Discover
========


.. image:: https://raw.githubusercontent.com/bitlogik/guardata/master/guardata/client/gui/rc/images/logos/guardata_vert.png
    :align: center

guardata is a secure and trustless cloud storage service, to share and sync your files with on-premise modern encryption.

Homepage: https://guardata.app


