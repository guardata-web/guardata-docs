guardata documentation
======================


Install requirements (Debian/Ubuntu)
```
# apt-get install -y python3-pip
# pip3 install -f sphinx sphinx_rtd_theme sphinx-intl
```


see http://www.sphinx-doc.org/en/master/usage/advanced/intl.html

Update translation
------------------

1) Generate native (English) pages

```
$ make -e html
```

2) Extract text and create/update .po files
```
$ make gettext
$ sphinx-intl update -p _build/locale -l fr
```

3) Translate .po files

4) Build .po -> .mo
```
$ make -e SPHINXOPTS="-D language='fr'" html
```
