#!/bin/bash

HTTP_PORT=2443

ROOTDIR=$(pwd)

trap_ctrlC() {
  echo "Restarting ... press again CTRL+C to kill"
  sleep 1 || exit 0
}

trap trap_ctrlC SIGINT SIGTERM

while true; do
  cd $ROOTDIR/docs
  rm -Rf _build
  make gettext
  make -e html
  mv _build/html _build/en
  sphinx-intl update -p _build/locale -l fr
  make -e SPHINXOPTS="-D language='fr'" html
  mv _build/html _build/fr
  cd _build
  echo "Press CTRL-C to restart (refresh files)"
  echo ""
  echo " --> http://$(wget -qO- https://api.ipify.org):$HTTP_PORT/en/ [ou/fr]"
  echo ""
  python3 -m http.server $HTTP_PORT
done

